function faireQuelqueChose(position) {
    //console.log("position = " + position) // Debug
    // Parti concernant le menu en général !
    if (position >= 1) { // Ajoute une couleur de fond au menu lorsque l'on scrool
        let topbar = document.getElementById('topbar_container');
        topbar.style.backgroundColor = "black";
    } else if (position <= 1) { // Le remet par defaut (transparent) lorsque l'on revient en haut de la page
        let topbar = document.getElementById('topbar_container');
        topbar.style.backgroundColor = "transparent";
    }

    if (screen.width >= 1920 ) { // Pour les grands écrans
        // Parti concernant chaque catégorie du menu !
        if (position > 950 && position < 1900) { // Des que l'on arrive à la rubrique Biographie
            let addBackground = document.getElementById('a_bio');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "biographie" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_bio');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 1901 && position < 2650) { // Des que l'on arrive à la rubrique Compétence
            let addBackground = document.getElementById('a_comp');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "compétence" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_comp');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 2651 && position < 3660) { // Des que l'on arrive à la rubrique PortFolio
            let addBackground = document.getElementById('a_pf');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "portfolio" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_pf');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 3661) { // Des que l'on arrive à la rubrique contact
            let addBackground = document.getElementById('a_cont');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "contact" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_cont');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }
    } else {
        // Parti concernant chaque catégorie du menu !
        if (position > 620 && position < 1219) { // Des que l'on arrive à la rubrique Biographie
            let addBackground = document.getElementById('a_bio');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "biographie" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_bio');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 1220 && position < 1789) { // Des que l'on arrive à la rubrique Compétence
            let addBackground = document.getElementById('a_comp');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "experience" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_comp');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 1790 && position < 2569) { // Des que l'on arrive à la rubrique PortFolio
            let addBackground = document.getElementById('a_pf');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "portfolio" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_pf');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }

        if (position >= 2570) { // Des que l'on arrive à la rubrique Contact
            let addBackground = document.getElementById('a_cont');
            addBackground.style.backgroundColor = "#008080"; // Applique un fond à "contact" dans la topbar
            addBackground.style.color = "white";
        } else { // Le retire lorsqu'on en sort
            let addBackground = document.getElementById('a_cont');
            addBackground.style.backgroundColor = "transparent";
            addBackground.style.color = "";
        }
    }

}

window.addEventListener('scroll', function () { // Fonction qui va capter la position de notre page
    let position = window.scrollY;
    if (screen.width >= 1024  ) { // Desactive les effets de positions pour les mobiles et tablettes
        faireQuelqueChose(position);
    }

});
