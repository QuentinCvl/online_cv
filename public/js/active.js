$('#hamburger-btn').click(function () { // Fonction du menu hamburger
    let menu = document.getElementById("hamburger-menu").parentNode;
    if (menu.style.display === "block") {
        menu.style.display = "none";
    } else {
        menu.style.display = "block";
    }
});

if (screen.width > 1024  ) { // Désactive cette fonctionnalité pour les téléphones et tablette
    // Event pour afficher le nom des images ( HTML5, CC3, etc ... ) lorsque la souris passe dessus
    $(".comp").mouseenter( function () {
        let show = $(this).children('.overlay');
        show.css('visibility', 'visible');
    });
    $(".comp").mouseleave( function () {
        let r = $(this).children('.overlay');
        r.css('visibility', 'hidden');
    });
}

/* -- Script pour le Portfolio -- */
// Va faire apparaitre la modal selectionnée
$('.openModal').click( function () {
    document.getElementById(this.id + "Modal").style.display = "block";
});
// Va fermer la modal actuellement ouverte
$(".close").click( function () {
    let idSplit = this.id.split('_'); // L'ID va être split afin de le faire ensuite corresponde avec la modal a fermer
    document.getElementById(idSplit[0] + "Modal").style.display = "none";
});

var slideIndex = 1;
showSlides(slideIndex);
showSlides2(slideIndex);
showSlides3(slideIndex);
showSlides4(slideIndex);
showSlides5(slideIndex);

// Controlleur de Slides ( -1 ou +1 en param )
function swapSlides(n) {
    showSlides(slideIndex += n);
}
function swapSlides2(n) {
    showSlides2(slideIndex += n);
}
function swapSlides3(n) {
    showSlides3(slideIndex += n);
}
function swapSlides4(n) {
    showSlides4(slideIndex += n);
}
function swapSlides5(n) {
    showSlides5(slideIndex += n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let captionText = document.getElementById("caption");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
    captionText.innerHTML;
}
function showSlides2(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides2");
    let captionText = document.getElementById("caption2");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    captionText.innerHTML;
    slides[slideIndex - 1].style.display = "block";
};
function showSlides3(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides3");
    let captionText = document.getElementById("caption3");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
    captionText.innerHTML;
};
function showSlides4(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides4");
    let captionText = document.getElementById("caption4");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
    captionText.innerHTML;
};
function showSlides5(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides5");
    let captionText = document.getElementById("caption5");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
    captionText.innerHTML;
};
